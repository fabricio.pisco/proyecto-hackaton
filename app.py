import pandas as pd
import streamlit as st
import plotly.express as px

st.set_page_config(
    page_title="Propuesta IAckaton",
    page_icon=":bar_chart:",
    layout="wide"
)

@st.cache_data
def load_data(file):
    data = pd.read_csv(file)
    return data

st.sidebar.header("Configuración")
page = st.sidebar.radio("Seleccionar Página", ["Top 6 Entidades Compradoras", "Número de Cuentas vs Dinero", "Promedio Consistencias Totales"])

#uploaded_file = st.sidebar.file_uploader("Seleccionar archivo CSV", type=["csv"])

#if uploaded_file is None:
#    st.info("Seleccione un archivo tipo .csv", icon="ℹ")
#    st.stop()

dc = load_data("new_tables/Top 6 Entity Contracts Count.csv")
eca = load_data("new_tables/Entity count amount.csv")
pit = load_data("new_tables/Promedio Inconsistencias Totales.csv")

#st.sidebar.write("Config. gráfica pastel:")
#minPastel = st.sidebar.number_input("Mínimo número de contratos:", min_value=0, value=450)

if page == "Top 6 Entidades Compradoras":
    with st.expander("Top 6 entidades compradoras (vista previa)"):
        st.dataframe(dc, use_container_width=True)
    with st.expander("Top 6 entidades compradoras (visualización de datos)"):
        st.header("Top 6 Compradores")

        dc['Cuenta'] = pd.to_numeric(dc['Cuenta'], errors='coerce')

        counts = dc.groupby('Nombre Entidad')['Cuenta'].sum().reset_index()
        #counts.loc[counts['Cuenta'] < minPastel, 'Nombre Entidad'] = f'Otros (Menos de {minPastel} contratos)'

        fig = px.pie(counts, values='Cuenta', names='Nombre Entidad', title=" ", height=300, width=400)

        fig.update_traces(textposition='outside', textinfo='percent+label')

        fig.update_layout(title_x=0.5,
                            margin=dict(l=20, r=20, t=30, b=0))

        st.plotly_chart(fig, use_container_width=True)

elif page == "Número de Cuentas vs Dinero":
    with st.expander("Número de Cuentas vs Dinero (vista previa)"):
        st.sidebar.subheader("Filtros de gráfica:")
        minIndex = st.sidebar.number_input("Índice mínimo:", min_value=0, value=0)
        maxIndex = st.sidebar.number_input("Índice máximo:", min_value=0, value=len(eca) - 1)
        minContratos = st.sidebar.number_input("Mínimo número de contratos:", min_value=0, value=0)
        maxContratos = st.sidebar.number_input("Máximo número de contratos:", min_value=minContratos, value=int(eca['Cuenta'].max()))
        minDinero = st.sidebar.number_input("Mínimo dinero gastado:", min_value=0, value=0)
        maxDinero = st.sidebar.number_input("Máximo dinero gastado:", min_value=minDinero, value=int(eca['Amount'].max()))
    
        filteredEca = eca[(eca['Cuenta'] >= minContratos) & (eca['Cuenta'] <= maxContratos) &
                      (eca['Amount'] >= minDinero) & (eca['Amount'] <= maxDinero) &
                      (eca.index >= minIndex) & (eca.index <= maxIndex)]
        
        st.dataframe(filteredEca, use_container_width=True)

    with st.expander("Número de Cuentas vs Dinero (visualización de datos)"):
        st.header("Visualización de Datos")
        st.write("Para ver el nombre de las entidades por índice, referirse a la vista previa.")

        col1, col2 = st.columns(2)

        with col1:
            st.subheader("Número de Contratos")
            fig1 = px.bar(filteredEca, x='Cuenta', y=filteredEca.index,
                          orientation='h', text='Cuenta', title=" ")
            fig1.update_traces(texttemplate='%{text}', textposition='inside')
            fig1.update_layout(
                xaxis_title=None,
                yaxis_title=None,
                showlegend=False,
                margin=dict(l=20, r=20, t=30, b=0),
            )
            st.plotly_chart(fig1, use_container_width=True)

        with col2:
            st.subheader("Suma de Dinero")
            fig2 = px.bar(filteredEca, x='Amount', y=filteredEca.index,
                          orientation='h', text='Amount', title=" ")
            fig2.update_traces(texttemplate='%{text}', textposition='inside')
            fig2.update_layout(
                xaxis_title=None,
                yaxis_title=None,
                showlegend=False,
                margin=dict(l=20, r=20, t=30, b=0),
            )
            st.plotly_chart(fig2, use_container_width=True)

else:
    with st.expander("Promedio Consistencias Totales por Proveedor (vista previa)"):
        st.subheader("NOTA:")
        st.write("- Para obtener los puntos se tomaron en cuenta: cantidad de dinero, estado del contrato, fecha de inicio")
        st.write("- Contar con cierto número de requisitos (no tener NULL como campo) le otorga cierta cantidad de puntos a una entidad (3/3 = 1pts, 2/3 = 0.5pts, 1/3 o 0/3 = 0pts)")
        st.write("- Se agrupó por nombre y se contó el número de contratos por proveedor")
        st.write("- En caso de contar con todos los campos, los puntos equivalen al número de contratos y a 100% de consistencia")
        st.write("  ")
        st.sidebar.subheader("Filtros de gráfica:")
        minIndex = st.sidebar.number_input("Índice mínimo:", min_value=0, value=0)
        maxIndex = st.sidebar.number_input("Índice máximo:", min_value=0, value=len(pit) - 1)
        minConsistencia = st.sidebar.number_input("Mínimo % de consistencia:", min_value=0, value=0)

        filteredPit = pit[(pit.index >= minIndex) & (pit.index <= maxIndex) & (pit['Promedio'] >= minConsistencia)]

        st.dataframe(filteredPit, use_container_width=True)

    with st.expander("Promedio Consistencias Totales por Proveedor (visualización de datos)"):
        st.header("Visualización de Datos")
        st.write("Para ver el nombre de las entidades por índice, referirse a la vista previa.")

        st.subheader("Consistencia total (%)")
        fig1 = px.bar(filteredPit, x='Promedio', y=filteredPit.index,
                        orientation='h', text='Promedio', title=" ")
        fig1.update_traces(texttemplate='%{text}', textposition='inside')
        fig1.update_layout(
            xaxis_title=None,
            yaxis_title=None,
            showlegend=False,
            margin=dict(l=20, r=20, t=30, b=0),
        )
        st.plotly_chart(fig1, use_container_width=True)
