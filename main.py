import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn import ensemble, model_selection, preprocessing, tree

# pd.options.display.max_rows = None
pd.options.display.max_columns = None
pd.set_option('display.max_colwidth', None)


def open_file(opcion = None):
    tender = pd.read_csv("datos_extraer/tender_2023.csv")
    awards = pd.read_csv("datos_extraer/awards_2023.csv")
    contracts = pd.read_csv('datos_extraer/contracts_2023.csv')
    extensions = pd.read_csv("datos_extraer/extensions_2023.csv")
    metadata = pd.read_csv("datos_extraer/metadata_2023.csv")
    planing = pd.read_csv("datos_extraer/planning_2023.csv")
    releases = pd.read_csv("datos_extraer/releases_2023.csv")
    suppliers = pd.read_csv("datos_extraer/suppliers_2023.csv")

    if opcion == 1:
        tender_list = tender.columns.tolist()
        awards_list = awards.columns.tolist()
        contracts_list = contracts.columns.tolist()
        extensions_list = extensions.columns.tolist()
        metadata_list = metadata.columns.tolist()
        planing_list = planing.columns.tolist()
        releases_list = releases.columns.tolist()
        suppliers_list = suppliers.columns.tolist()

        print("\nTender \n", tender_list)
        print("\nawards \n", awards_list)
        print("\ncontracts\n", contracts_list)
        print("\nextensions\n", extensions_list)
        print("\nmetadata\n", metadata_list)
        print("\nplaning\n", planing_list)
        print("\nreleases\n", releases_list)
        print("\nsuppliers\n", suppliers_list)
    elif opcion == 2:
        print(tender.count(), "\n----------------------------------\n",
              awards.count(), "\n----------------------------------\n",
              contracts.count(), "\n----------------------------------\n",
              extensions.count(), "\n----------------------------------\n",
              metadata.count(), "\n----------------------------------\n",
              planing.count(), "\n----------------------------------\n",
              releases.count(), "\n----------------------------------\n",
              suppliers.count())
    elif opcion == None:
        tender_list = tender.columns.tolist()
        awards_list = awards.columns.tolist()
        contracts_list = contracts.columns.tolist()
        extensions_list = extensions.columns.tolist()
        metadata_list = metadata.columns.tolist()
        planing_list = planing.columns.tolist()
        releases_list = releases.columns.tolist()
        suppliers_list = suppliers.columns.tolist()

        print("\nTender \n", tender_list)
        print("\nawards \n", awards_list)
        print("\ncontracts\n", contracts_list)
        print("\nextensions\n", extensions_list)
        print("\nmetadata\n", metadata_list)
        print("\nplaning\n", planing_list)
        print("\nreleases\n", releases_list)
        print("\nsuppliers\n", suppliers_list)

        print("\n----------------------------------\n", tender.count(), "\n----------------------------------\n",
              awards.count(), "\n----------------------------------\n",
              contracts.count(), "\n----------------------------------\n",
              extensions.count(), "\n----------------------------------\n",
              metadata.count(), "\n----------------------------------\n",
              planing.count(), "\n----------------------------------\n",
              releases.count(), "\n----------------------------------\n",
              suppliers.count())


def entity_contracts_count():
    tender = pd.read_csv("datos_extraer/tender_2023.csv")
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.strip().str.lower()

    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.replace('gobierno autonomo descentralizado',
                                                                                'GAD ')
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.replace('empresa publica mancomunada para la',
                                                                                'EPM ')
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.replace('empresa publica municipal mancomunada',
                                                                                'EPMM ')
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.replace('empresa publica', 'EP ')
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.replace(
        'de comercializacion e industrializaciontransporte multimodal y turismo del gobierno autonomdescentralizado ',
        ' CI ')
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.replace(
        'direccion distrital 23d01 salud alluriquín, luz de américa, esfuerzo, toachi, périferia 1, rio verde, santo domingo, zaracay, río toachí, chiguilpe, san jacinto del búa, valle hermoso, puerto limón, periferia 2, abraham calazacón, bombolí, la concor',
        'DD muchas zonas')

    cuenta = tender['procuringEntity_name'].value_counts().reset_index()
    cuenta.columns = ['Nombre Entidad', 'Cuenta']
    cuenta.to_csv('new_tables/Entity Contracts Count.csv', index=False)
    print(cuenta)
    return cuenta


def top_6_entity_contracts_count():
    tender = pd.read_csv("datos_extraer/tender_2023.csv")
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.strip().str.lower()

    v_to_filter = [
        "coordinacion zonal 4",
        "gobierno autonomo descentralizado del distrito metropolitano de quito",
        "corporacion electrica del ecuador celec ep.",
        "servicio de rentas internas",
        "instituto ecuatoriano de seguridad social",
        "subsecretaria de educacion del distrito metropolitano de quito"
    ]

    filtro = tender['procuringEntity_name'].isin(v_to_filter)
    filtrada = tender[filtro]

    filtrada['procuringEntity_name'] = filtrada['procuringEntity_name'].str.replace('gobierno autonomo descentralizado',
                                                                                'GAD ')
    filtrada['procuringEntity_name'] = filtrada['procuringEntity_name'].str.replace('corporacion electrica del ecuador', '')
    filtrada['procuringEntity_name'] = filtrada['procuringEntity_name'].str.replace(
        'subsecretaria de educacion del distrito metropolitano ', 'SEDM ')

    cuenta_filtrada = filtrada['procuringEntity_name'].value_counts().reset_index()
    cuenta_filtrada.columns = ['Nombre Entidad', 'Cuenta']
    cuenta_filtrada.to_csv('new_tables/Top 6 Entity Contracts Count.csv', index=False)
    print(cuenta_filtrada)


def entity_contracts_amount():
    tender = pd.read_csv("datos_extraer/tender_2023.csv")
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.strip().str.lower()

    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.replace('gobierno autonomo descentralizado',
                                                                                'GAD ')
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.replace('empresa publica mancomunada para la',
                                                                                'EPM ')
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.replace('empresa publica municipal mancomunada',
                                                                                'EPMM ')
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.replace('empresa publica', 'EP ')
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.replace(
        'de comercializacion e industrializaciontransporte multimodal y turismo del gobierno autonomdescentralizado ',
        ' CI ')
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.replace(
        'direccion distrital 23d01 salud alluriquín, luz de américa, esfuerzo, toachi, périferia 1, rio verde, santo domingo, zaracay, río toachí, chiguilpe, san jacinto del búa, valle hermoso, puerto limón, periferia 2, abraham calazacón, bombolí, la concor',
        'DD muchas zonas')

    amount = tender.groupby('procuringEntity_name')['value_amount'].agg('sum').reset_index()
    amount.columns = ['Nombre Entidad', 'Amount']
    amount.to_csv('new_tables/Entity Contracts Amount.csv', index=False)
    print(amount)
    return amount


def top_6_entity_contracts_amount():
    tender = pd.read_csv("datos_extraer/tender_2023.csv")
    tender['procuringEntity_name'] = tender['procuringEntity_name'].str.strip().str.lower()

    g_to_filter = [
        "empresa pública de hidrocarburos del ecuador ep petroecuador",
        "corporacion electrica del ecuador celec ep.",
        "ministerio de educacion",
        "empresa eléctrica pública estratégica corporación nacional de electricidad cnel ep",
        "digmat",
        "gobierno autonomo descentralizado del distrito metropolitano de quito"
    ]

    filtro = tender['procuringEntity_name'].isin(g_to_filter)
    filtrada = tender[filtro]

    filtrada['procuringEntity_name'] = filtrada['procuringEntity_name'].str.replace(
        'gobierno autonomo descentralizado','GAD ')
    filtrada['procuringEntity_name'] = filtrada['procuringEntity_name'].str.replace(
        'corporacion electrica del ecuador', '')
    filtrada['procuringEntity_name'] = filtrada['procuringEntity_name'].str.replace(
        'subsecretaria de educacion del distrito metropolitano ', 'SEDM ')

    amout_filtrada = filtrada.groupby('procuringEntity_name')['value_amount'].agg('sum').reset_index()
    amout_filtrada.columns = ['Nombre Entidad', 'Suma Valores']
    amout_filtrada = amout_filtrada.sort_values(by='Suma Valores', ascending=False)
    amout_filtrada.to_csv('new_tables/Top 6 Entity Contracts Amount.csv', index=False)
    print(amout_filtrada)


def join_awards_tender():
    suppliers = pd.read_csv("datos_extraer/suppliers_2023.csv")
    tender = pd.read_csv("datos_extraer/tender_2023.csv")
    joined = pd.merge(tender, suppliers, on='ocid', how='outer')
    columns = ['procuringEntity_name', 'value_amount', 'name']
    joined = joined[columns]
    joined.to_csv('new_tables/Joined Entities x Suppliers.csv', index=False)
    return joined


def top_6_at_eca():
    joined = join_awards_tender()
    joined['procuringEntity_name'] = joined['procuringEntity_name'].str.strip().str.lower()
    joined['name'] = joined['name'].str.strip().str.lower()

    filter = [
        "empresa pública de hidrocarburos del ecuador ep petroecuador",
        "corporacion electrica del ecuador celec ep.",
        "ministerio de educacion",
        "empresa eléctrica pública estratégica corporación nacional de electricidad cnel ep",
        "digmat",
        "gobierno autonomo descentralizado del distrito metropolitano de quito"
    ]
    filtro = joined['procuringEntity_name'].isin(filter)
    filtrada = joined[filtro]
    filtrada['procuringEntity_name'] = filtrada['procuringEntity_name'].str.replace(
        'gobierno autonomo descentralizado', 'GAD ')
    filtrada['procuringEntity_name'] = filtrada['procuringEntity_name'].str.replace(
        'corporacion electrica del ecuador', '')
    filtrada['procuringEntity_name'] = filtrada['procuringEntity_name'].str.replace(
        'empresa eléctrica pública estratégica corporación nacional de electricidad ', '')
    filtrada['procuringEntity_name'] = filtrada['procuringEntity_name'].str.replace(
        'empresa pública de hidrocarburos del ecuador ep ', '')
    monto_filtrada = filtrada.groupby(['procuringEntity_name', 'name'])['value_amount'].agg('sum').reset_index()
    monto_filtrada.columns = ['Nombre Entidad', 'Nombre Proveedor', 'Suma Valores']
    monto_filtrada.to_csv('new_tables/Top 6 Entidad x Proveedor.csv', index=False)
    print(monto_filtrada)


def entity_count_amount():
    amount = entity_contracts_amount()
    count = entity_contracts_count()
    conmbinado = pd.merge(count, amount, on='Nombre Entidad', how='outer')
    conmbinado = conmbinado.fillna(0)
    conmbinado.to_csv('new_tables/Entity count amount.csv', index=False)
    print(conmbinado)


def join_suppliers_contracts():
    suppliers = pd.read_csv("datos_extraer/suppliers_2023.csv")
    contracts = pd.read_csv('datos_extraer/contracts_2023.csv')
    joined = pd.merge(suppliers, contracts, on='ocid', how='inner')
    columnas_usar = ['name', 'id_y', 'status', 'contractPeriod_startDate', 'amount']
    joined = joined[columnas_usar]
    return joined


def check_consistenciy():
    checkear = join_suppliers_contracts()
    nulos = checkear.isnull().sum(axis=1)
    checkear['cumplimiento'] = np.where(nulos >= 2, 0, np.where(nulos == 1, 0.5, 1))
    # Tabla con nombre proveedores y puntos
    conteo = checkear.groupby('name')['cumplimiento'].agg('sum').reset_index()
    conteo.columns = ['Proveedor', 'Puntos']
    # Tabla con nombre proveedores y contratos totales
    proveedores = checkear['name'].value_counts().reset_index()
    proveedores.columns = ['Proveedor', 'Contratos Totales']
    # Union de Tablas anteriores para sacar promedio
    promedio_consistencias = pd.merge(conteo, proveedores, on='Proveedor', how='inner')
    promedio_consistencias['Promedio'] = (
            promedio_consistencias['Puntos'] / promedio_consistencias['Contratos Totales'] * 100)
    promedio_consistencias = promedio_consistencias.sort_values(by='Contratos Totales', ascending=False)
    promedio_consistencias.to_csv('new_tables/Promedio Inconsistencias Totales.cvs', index=False)

    print(promedio_consistencias)

    # print(checkear)


if __name__ == '__main__':
    # Mostrar Datos Consola
    # Sin enviar nada se imprimen la opcion 1 y 2, Opcion 1 Cuenta, Opcion 2 = Titulo Columnas,
    open_file()

    # Datos Graficas
    # top_6_entity_contracts_amount()
    # top_6_entity_contracts_count()
    # entity_contracts_count()
    # entity_contracts_amount()
    # top_6_at_eca()

    # Datos Entidades
    # entity_count_amount()

    # Promedio de Datos Consistentes
    # check_consistenciy()
